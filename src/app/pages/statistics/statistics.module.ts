import { NgModule } from "@angular/core";
import { StatisticsPageComponent } from './statistics.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        StatisticsPageComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: StatisticsPageComponent
            }
        ])
    ]
})

export class StatisticsPageModule {}