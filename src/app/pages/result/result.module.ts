import { NgModule } from "@angular/core";
import { ResultPageComponent } from './result.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        ResultPageComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: ResultPageComponent
            }
        ])
    ]
})

export class ResultPageModule {}