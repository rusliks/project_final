import { NgModule } from "@angular/core";
import { BudgetPageComponent } from './budget.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        BudgetPageComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: BudgetPageComponent
            }
        ])
    ]
})

export class BudgetPageModule {}