import { NgModule } from "@angular/core";
import {RatePageComponent } from './rate.component';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

@NgModule({
    declarations: [
        RatePageComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {
                path: '',
                component: RatePageComponent
            }
        ])
    ]
})

export class RatePageModule {}