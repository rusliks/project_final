import { NgModule } from "@angular/core";
import { Route } from "@angular/route";
import { RouterModule } from '@angular/router';

const routes: Route[] = [
    {
        path: '',
        redirectTo: 'budget',
        pathMatch: 'full'
    },
    {
        path: 'budget',
        loadChildren: './pages/budget/budget.module#BudgetPageModule'
    },
    {
        path: 'account',
        loadChildren: './pages/account/account.module#AccountPageModule'
    },
    {
        path: 'auth',
        loadChildren: './pages/auth/auth.module#AuthPageModule'
    },
    {
        path: 'rate',
        loadChildren: './pages/rate/rate.module#RatePageModule'
    },
    {
        path: 'result',
        loadChildren: './pages/result/result.module#ResultPageModule'
    },
    {
        path: 'statistics',
        loadChildren: './pages/statistics/statistics.module#StatisticsPageModule'
    }
];

@NgModule({
    imports: [
        RouterModule.forRoot(routes)
    ],
    exports: [
        RouterModule
    ]
})

export class AppRoutingModule { }